require('dotenv').config();

module.exports = {
	NODE_ENV: '"production"',
	APP_VERSION: '"0.0.1"',
	APP_API_URL: `"${process.env.APP_API_URL}"`,
};