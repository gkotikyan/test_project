// const auth = {
//   login: `${process.env.APP_API_URL}/auth`
// };

const clientURL = `http://${process.env.APP_API_URL}/api/clients`;
const client = {
  get: {
    url: clientURL,
    method: 'GET'
  },
  create: {
    url: clientURL,
    method: 'POST'
  },
  remove: {
	  url: clientURL,
	  method: 'DELETE'
  },
  update: {
	  url: clientURL,
	  method: 'PATCH'
  },
	emailVerify: {
  	url: `${clientURL}/email-verify`,
		method: 'GET'
	}
};

const providerURL = `http://${process.env.APP_API_URL}/api/providers`;
const provider = {
	get: {
		url: providerURL,
		method: 'GET'
	},
	create: {
		url: providerURL,
		method: 'POST'
	},
	remove: {
		url: providerURL,
		method: 'DELETE'
	},
	update: {
		url: providerURL,
		method: 'PATCH'
	}
};

export {
  client,
	provider
};
