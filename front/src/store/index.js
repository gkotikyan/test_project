import Vue from 'vue';
import Vuex from 'vuex';

import clients from './modules/clients';
import providers from './modules/providers';

Vue.use(Vuex);

export default new Vuex.Store({
  strict: true,
  modules: {
    clients,
	  providers
  }
});
