const add = (state, payload) => {
  state.items.push(payload);
};

const get = (state, payload) => {
  state.items = payload;
};

const remove = (state, payload) => {
  state.items.splice(state.items.findIndex(item => payload === item._id), 1);
};

const update = (state, payload) => {
	const index = state.items.findIndex(item => payload.id === item._id);
	const items = Object.assign([], state.items);
	items[index] = payload.data;
	state.items = items;
};

export default {
  add,
  get,
  remove,
	update
};
