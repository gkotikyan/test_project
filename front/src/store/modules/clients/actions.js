/* eslint-disable no-mixed-spaces-and-tabs */
import axios from 'axios';
import { client } from '@/requests';

const get = async ({ commit }) => {
  try {
    const response = await axios(client.get);
    const { data } = response;
    commit('get', data);
  } catch (error) {
    console.log(error);
  }
};

const remove = async ({ commit }, payload) => {
  try {
  	const removeRequest = Object.assign({}, client.remove);
  	removeRequest.url = `${removeRequest.url}/${payload}`;
	  await axios(removeRequest);
    commit('remove', payload);
  } catch (error) {
  }
};

const update = async ({ commit }, payload) => {
	try {
		const updateRequest = Object.assign({}, client.update);
		updateRequest.url = `${updateRequest.url}/${payload.id}`;
		updateRequest.data = payload.data;
		await axios(updateRequest);
		commit('update', payload);
	} catch (error) {
	}
};

const save = async ({ commit }, payload) => {
  try {
    client.create.data = payload;
    const response = await axios(client.create);
    const { data } = response;
    commit('add', data);
  } catch (error) {
    console.log(error);
  }
};

const emailVerification = async ({ commit }, payload) => {
  try {
    client.emailVerify.params = {
	    	email: payload
    };
    const response = await axios(client.emailVerify);
    const { data } = response;
    return data;
  } catch (error) {
    console.log(error);
  }
};

export default {
  get,
  save,
  remove,
	update,
	emailVerification
};
