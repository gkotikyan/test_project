/* eslint-disable no-mixed-spaces-and-tabs */
import axios from 'axios';
import { provider } from '@/requests';

const get = async ({ commit }) => {
  try {
    const response = await axios(provider.get);
    const { data } = response;
    commit('get', data);
  } catch (error) {
    console.log(error);
  }
};

const remove = async ({ commit }, payload) => {
  try {
  	const removeRequest = Object.assign({}, provider.remove);
  	removeRequest.url = `${removeRequest.url}/${payload}`;
	  await axios(removeRequest);
    commit('remove', payload);
  } catch (error) {
  }
};

const update = async ({ commit }, payload) => {
	try {
		const updateRequest = Object.assign({}, provider.update);
		console.log(provider.update);
		updateRequest.url = `${updateRequest.url}/${payload.id}`;
		updateRequest.data = payload.data;
		await axios(updateRequest);
		commit('update', payload);
	} catch (error) {
	}
};

const save = async ({ commit }, payload) => {
  try {
    provider.create.data = payload;
    console.log(provider.create);
    const response = await axios(provider.create);
    const { data } = response;
    commit('add', data);
  } catch (error) {
    console.log(error);
  }
};

export default {
  get,
  save,
  remove,
	update
};
