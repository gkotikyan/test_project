# test_project

## Usage for development

1) Create .env files in front
```
echo -e "APP_API_URL='localhost:3000'" > ./front/.env
echo -e "JWT_SECRET=Fy5wBCOLbF\nDB_HOST=localhost" > ./server/.env
```

2) Run front NPM install
```
cd ./front
npm i
```

3) Run server NPM install
```
cd ../server
npm i
```

4) Run server in the server directory
```
npm start
```

5) Run dev front in the front directory
```
npm start
```

# Usage for production

1) Create .env files in front
set following variables
APP_API_URl property should be back-end host
JWT_SECRET jwt secret
DB_HOST db host
```
echo -e "APP_API_URL='localhost:3000'" > ./front/.env
echo -e "JWT_SECRET=Fy5wBCOLbF\nDB_HOST=localhost" > ./server/.env
```

2) Run front NPM install
```
cd ./front
npm i
```

3) Run server NPM install
```
cd ../server
npm i
```

4) Run server in the server directory
```
npm start
```

5) Run dev front in the front directory
```
npm run build
```

6) Serve dist/index.html file which has been created after running 5-th step
