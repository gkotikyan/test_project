let jwt = require('jsonwebtoken');
let config = require('../config');

module.exports = {
    decode : token => {
        return new Promise((resolve, reject) => {
            jwt.verify(token.split(' ')[1], config.jwt.secret, function (err, decoded) {
                if (err) {
                    console.log(err);
                    reject(err);
                    return;
                }
                resolve({
                    bearer : token,
                    userId : decoded.userId
                });
            })
        });
    },
}