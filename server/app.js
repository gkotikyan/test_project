/**
 * Node Modules
 */
const express = require('express');
const config = require('./config');
const logger = require('morgan');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const token = require('./custom_modules/token');
const cors = require('cors');
require('dotenv').config(__dirname);

// Mongo connection
mongoose.connect(config.mongo_conf.url, config.mongo_conf.options);

const auth = require('./modules/auth/route');
const clients = require('./modules/clients/route');
const providers = require('./modules/providers/route');

const app = express();

// Logger
app.use(logger('dev'));

// Parse income body
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// Enable CORS
app.use(cors());

// Ensure user is authenticated

// Routes
app.use('/auth', auth);
app.use('/api/clients', clients);
app.use('/api/providers', providers);

// Catch 404 and forward to error handler
app.use((req, res, next) => {
  const err = new Error('Not Found');
  err.code = 404;
  next(err);
});

// Error handler
app.use((err, req, res, next) => {
  res.status(err.code || 500);
  const response = {error: err.message}
  if (!process.env.NODE_ENV || process.enc.NODE_ENV === "development") {
    response.stack = err.stack
  }
  res.json(response)
});

module.exports = app;
