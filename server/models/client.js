let mongoose = require('mongoose');

let Schema = mongoose.Schema({
    name: {
        type: String
    },
    email: {
        type: String,
        unique: true
    },
    phone: {
        type: String,
        unique: true
    },
    providers: [{
        type: mongoose.Schema.ObjectId,
        ref: 'providers'
    }]
}, {
    versionKey: false
});


let Model = mongoose.model('clients', Schema);

module.exports = Model;
