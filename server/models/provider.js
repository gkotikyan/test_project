let mongoose = require('mongoose');

let Schema = mongoose.Schema({
    name: {
        type: String
    }
}, {
    versionKey: false
});

Schema.post('remove', function(doc) {
	console.log('%s has been removed', doc._id);
	this.model('clients').update({}, { $pull: { providers: doc._id } }, {multi: true});
});

let Model = mongoose.model('providers', Schema);

module.exports = Model;
