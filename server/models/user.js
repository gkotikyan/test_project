let mongoose = require('mongoose');
let bcrypt = require('bcrypt');
let jwt = require('jsonwebtoken');
const config = require('../config');
const saltRounds = 10;

let Schema = mongoose.Schema({
    first_name: {
        type: String
    },
    last_name: {
        type: String
    },
    password: {
        type: String
    },
    email: {
        type: String,
        unique: true
    },
    username: {
        type: String,
        unique: true
    }
}, {
    versionKey: false
});


let Model = mongoose.model('users', Schema);


// Default User
Model.seed = async () => {
    try {
        const password = 'adminadmin';
        const hash = await bcrypt.hash(password, saltRounds)
        
        const data = {
            first_name: 'admin',
            last_name: 'admin',
            password: password,
            email: 'admin@admin.com',
            username: 'admin'
        }
    
        await Model.findByIdAndUpdate({}, data, { upsert: true })
    } catch (error) {
        throw error
    }
}

Model.login = async (username, password) => {
    try {
        const error = new Error('User username or password is incorrect');
        error.code = 401;
            
        const result = await Model.findOne({username: username}).exec()
        if (!result) {
            throw error;
        }
        const isMatch = await bcrypt.compare(password, result.password);
        if (isMatch) {
            const token = jwt.sign({
                exp: Math.floor(Date.now() / 1000) + (60 * 60 * 60),
                userId: result.id
            }, config.jwt.secret);

            return {
                token: token,
                user: {
                    username: result.username,
                    email: result.email,
                    first_name: result.first_name,
                    last_name: result.last_name
                }
            }
        } else {
            throw error;
        }
        
    } catch (error) {
        throw error
    }
};

module.exports = Model;