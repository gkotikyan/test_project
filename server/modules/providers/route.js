const express = require('express');
const router = express.Router();
const controller = require('./controller');
const { check } = require('express-validator/check');

// Get the provider
router.get('/', controller.get);

// Create the provider
router.post('/', [
	check('name').exists().withMessage('name must not be empty'),
], controller.create);

// Update the provider
router.patch('/:id', [
	check('name').exists().withMessage('name must not be empty')
], controller.update);

// Delete the provider
router.delete('/:id', controller.remove);

module.exports = router;
