const Provider = require('../../models/provider');
const { validationResult } = require('express-validator/check');

/**
 * 
 * @param {*} req 
 * @param {*} res 
 */
const get = async (req, res, next) => {
    try {
        const response = await Provider.find().exec();
        res.json(response);
    } catch (error) {
        throw error;
    }
};

/**
 * 
 * @param {*} req 
 * @param {*} res 
 */
const create = async (req, res, next) => {
    try {
		    const errors = validationResult(req);
		    if (!errors.isEmpty()) {
			    return res.status(422).json({ errors: errors.array() });
		    }

		    const { name } = req.body;
        const provider =  new Provider({
            name
        });
        await provider.save();
        res.json(provider);
    } catch (error) {
        throw error;
    }
};

/**
 * 
 * @param {*} req 
 * @param {*} res 
 */
const update = async (req, res, next) => {
    try {
		    const errors = validationResult(req);
		    if (!errors.isEmpty()) {
			    return res.status(422).json({ errors: errors.array() });
		    }

		    const { id } = req.params;
        const { name } = req.body;
        const data = {
            name
        };
        const response = await Provider.update({_id: id}, {$set: data}).exec();
        res.json(response);
    } catch (error) {
        throw error;
    }
};

/**
 * 
 * @param {*} req 
 * @param {*} res 
 */
const remove = async (req, res, next) => {
    try {
        const { id } = req.params;
		    const provider = new Provider({_id: id});
		    const response = await provider.remove();
        res.json({response});
    } catch (error) {
        throw error;
    }
};

module.exports = {
    get,
    create,
    update,
    remove
};
