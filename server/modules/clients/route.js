const express = require('express');
const router = express.Router();
const controller = require('./controller');
const Client = require('../../models/client');
const { check } = require('express-validator/check');

// Get the clients
router.get('/', controller.get);

// Create the clients
router.post('/', [
	check('name').exists().withMessage('name must not be empty'),
	check('email').isEmail().withMessage('email must be valid'),
	check('email').custom( async value => {
		try {
			const client = await Client.findOne({email: value});
			if (client) {
				throw new Error("Email exists");
			}
		} catch (e) {
			throw e
		}
	})
], controller.create);

// Update the clients
router.patch('/:id', [
	check('name').exists().withMessage('name must not be empty'),
	check('email').isEmail().withMessage('email must be valid')
], controller.update);

// Delete the clients
router.delete('/:id', controller.remove);

// Email erification
router.get('/email-verify', controller.verifyEmail);

module.exports = router;
