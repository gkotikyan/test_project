const Client = require('../../models/client');
const { validationResult } = require('express-validator/check');
/**
 * 
 * @param {*} req 
 * @param {*} res 
 */
const get = async (req, res, next) => {
    try {
        const response = await Client.find().populate('providers').exec();
        res.json(response);
    } catch (error) {
        throw error;
    }
};

/**
 * 
 * @param {*} req 
 * @param {*} res 
 */
const create = async (req, res, next) => {
    try {
		    const errors = validationResult(req);
		    if (!errors.isEmpty()) {
			    return res.status(422).json({ errors: errors.array() });
		    }

		    const { name, email, phone, providers } = req.body;
        const client =  new Client({
            name,
            email,
            phone,
            providers
        });
        const data = await client.save();
				const response = await Client.findById(data._id).populate('providers').exec();
        res.json(response);
    } catch (error) {
        throw error;
    }
};

/**
 * 
 * @param {*} req 
 * @param {*} res 
 */
const update = async (req, res, next) => {
    try {
		    const errors = validationResult(req);
		    if (!errors.isEmpty()) {
			    return res.status(422).json({ errors: errors.array() });
		    }

		    const { id } = req.params;
        const { name, email, phone, providers } = req.body;
        const data = {
            name,
            email,
            phone,
            providers
        };
        const response = await Client.update({_id: id}, {$set: data}).exec();
        res.json(response);
    } catch (error) {
        throw error;
    }
};
/**
 * 
 * @param {*} req 
 * @param {*} res 
 */
const remove = async (req, res, next) => {
    try {
        const { id } = req.params;
        const response = await Client.remove({id}).exec();
        res.json(response);
    } catch (error) {
        throw error;
    }
};

/**
 *
 * @param {*} req
 * @param {*} res
 */
const verifyEmail = async (req, res, next) => {
    try {
        const { email } = req.query;
        const data = await Client.findOne({email: email}).exec();
        const response = !!data;
        res.json(response);
    } catch (error) {
        throw error;
    }
};

module.exports = {
    get,
    create,
    update,
    remove,
		verifyEmail
};
