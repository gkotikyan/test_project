const User = require('../../models/user');

const login = async (req, res, next) => {
    try {
        const { username, password } = req.body;
        const response = await User.login(username, password)
        res.json(response);
    } catch (error) {
        next(error)
    }
}

module.exports = {
    login
};
