module.exports = {
    jwt: {
        secret: process.env.JWT_SECRET
    },
    mongo_conf: {
        url: `mongodb://${process.env.DB_HOST || 'localhost'}/sp`,
        options: {
            server: {
                auto_reconnect : true,
                reconnectTries : 17280,
                reconnectInterval : 5000
            }
        }
    },
};